CREATE TABLE IF NOT EXISTS intoxicacao_db.99_banco_agentes_intoxicantes (
	grupo varchar(1024),
	classe varchar(1024),
	subclasse varchar(1024),
	substancia varchar(1024),
	nome_popular_comercial varchar(1024)
);
