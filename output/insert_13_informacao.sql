INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4487', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4578', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4609', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5032', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5159', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5159', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5187', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5353', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5411', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5561', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5604', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5735', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre riscos e controle de caramujo africano.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5771', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre casos de caes vitimas de caramujo africano.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5802', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5802', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5822', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5955', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5955', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6236', 
	'Informacao Ocupacional', 
	'Dados tecnicos sobre seguranca e manuseio de material (ficha tecnica)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6273', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6273', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6274', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6401', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6401', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6414', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6422', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6601', 
	'Metais', 
	'Informacoes sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6629', 
	'Outros', 
	'Informacoes sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6691', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6745', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Informacoes de medicacoes que podem e/ou nao podem ser usadas em crianca de 6 anos com frutosemia. Informacoes sobre medicacoes de uso mais geral e corriqueiro, como analgesicos, antinflamatorios, antiemeticos, etc. Procurei exaustivamente na web, nao encontrei. Outra coisa seria um site de informacoes para pais e cuidadores.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6778', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6943', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7478', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7567', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7575', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7594', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7758', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7773', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7782', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7918', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7923', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7940', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8065', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8097', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8125', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8128', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8132', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8299', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8601', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8760', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8773', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8900', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8957', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8962', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8967', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8974', 
	'Identificacao de Substancias', 
	'Identificacao de formas farmaceuticas solidas (sob prescricao, isentas de prescricao, suplemento dietetico/ervas, ilicita)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9055', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9227', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9231', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9244', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9289', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9289', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9712', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9712', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9763', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9855', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9903', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9992', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10145', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10192', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10345', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10365', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Recolhimento de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10495', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10555', 
	'Drogas de Abuso', 
	'Efeitos de substancias ilicitas - sem vitima(s) identificada(s)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10704', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10705', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10777', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10907', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10923', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10980', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11220', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Enfermeira gostaria de saber se depois de quase 4 meses  de uma picada de aranha marrom, o veneno do animal pode causar hemolise. Ha um caso assim no municipio e esquipe esta em duvida; '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11412', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11904', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11907', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11954', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12084', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12141', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12353', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12458', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12975', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13114', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13119', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'estatistica dos casos atendidos no CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13221', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13221', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13279', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13306', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13307', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13347', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13347', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13371', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13377', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13441', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13444', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14210', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14216', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14221', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14370', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14412', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15040', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15236', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15265', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15364', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15367', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15392', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15662', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15746', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15793', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15797', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Solicitacao de panfletos informativos para realizacao de atividade de prevencao.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15802', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15802', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15927', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Monografia de Etanol e Metanol'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15927', 
	'Identificacao de Substancias', 
	'Identificacao de substancias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15943', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16007', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16412', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16412', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16418', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16473', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16780', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16984', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16986', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17110', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17213', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17946', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18083', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18230', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18234', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18234', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18279', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18296', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18316', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18788', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18790', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18849', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18920', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Solicita informacoes sobre risco de picada de aranha para lactante.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18926', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18930', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18930', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19113', 
	'Metais', 
	'Informacoes sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19297', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19405', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19423', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19449', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19455', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19459', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19465', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19472', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19806', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19827', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19827', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19831', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19940', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19942', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20034', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20202', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20256', 
	'Medicamentos', 
	'Monitorizacao terapeutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20683', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20688', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20708', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20716', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20847', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20927', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21039', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21058', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21058', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21325', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21335', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21358', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21470', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21470', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21514', 
	'Medicamentos', 
	'Monitorizacao terapeutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21521', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21833', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21833', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22091', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22226', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22341', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22352', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22352', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22360', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22381', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22420', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22503', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22520', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22520', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22899', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23308', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23313', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Dose Toxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23378', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23378', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23379', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23382', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23929', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23942', 
	'Alimentos', 
	'Intoxicacao alimentar - sem vitima(s) identificada(2)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24010', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24010', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24035', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24135', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'risco de toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24157', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24223', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24275', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24385', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24385', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24397', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24489', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24489', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24502', 
	'Metais', 
	'Informacoes sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24570', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24701', 
	'Outros', 
	'Informacao sobre doencas gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24737', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24737', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24768', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24775', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24873', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25081', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25165', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25263', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25267', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25430', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25430', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25692', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25763', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25763', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25852', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25874', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26377', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26379', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26379', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26642', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26642', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26823', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26896', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26896', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27130', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27191', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27197', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27197', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27216', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27383', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27497', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27548', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27553', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27553', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27665', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27665', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27703', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27703', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27771', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27809', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27809', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27866', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27900', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28153', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28160', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28181', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28220', 
	'Drogas de Abuso', 
	'Efeitos de substancias ilicitas - sem vitima(s) identificada(s)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28249', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28266', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28269', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28338', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28338', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28362', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28366', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28372', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28434', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28442', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28585', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'relacao da toxicidade do SALOX na gestacao.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28882', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28887', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28904', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28939', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28944', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28962', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28962', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28977', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28992', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29449', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29791', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29892', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Pcte queria saber diagnostico clinico.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30386', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30610', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30976', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30976', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31066', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31066', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31183', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31183', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31199', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31199', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31269', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31278', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31441', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31441', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31448', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31463', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31899', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31899', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31995', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Informacoes sobre automedicacao'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31998', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32000', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32030', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32463', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32575', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32745', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32745', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32779', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32863', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33358', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33358', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33373', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33451', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33473', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33747', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'vaselina liquida'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33775', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33833', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34142', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34250', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Farmacologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34369', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Exames ocupacionais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34376', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34891', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Dados estatisticos de intoxicacoes por medicamentos '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34901', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35161', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35171', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35281', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35416', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35416', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35638', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35638', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35911', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36115', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36134', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36134', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36237', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Gadolinio - Meio de Contraste'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36237', 
	'Medicamentos', 
	'Farmacocinetica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36398', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36408', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36418', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36456', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37062', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37440', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37440', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37445', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37445', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37545', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'estatistica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37552', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37893', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37893', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38111', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38307', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38353', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38457', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38457', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38458', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38458', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38902', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38902', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38945', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38945', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39052', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39052', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39058', 
	'Medicamentos', 
	'Estabilidade/armazenamento', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39058', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39188', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39188', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39195', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39519', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Notificacao compulsoria de intoxicacao por medicamentos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39552', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40133', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40148', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40176', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40176', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40253', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40274', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Informacao sobre o risco de evolui com sindrome alcalino lactea apos o uso de Bicarbonato de sodio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40334', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40337', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40437', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40437', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40441', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40441', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40445', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40544', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40564', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Solicitacao de material educativo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40564', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Solicitacao de material educativo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40582', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40582', 
	'Informacao Toxicologica', 
	'Teratogenicidade', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40628', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40747', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40815', 
	'Identificacao de Substancias', 
	'Identificacao de substancias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40818', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40818', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40856', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40856', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40858', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40858', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40971', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40974', 
	'Medicamentos', 
	'Monitorizacao terapeutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40984', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41009', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41026', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41026', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41038', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41038', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41138', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41607', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41662', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41717', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41717', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41826', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41890', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41890', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42009', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42009', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42026', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42026', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42096', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42146', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Disponibilidade de SAEl'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42202', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42202', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42247', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42247', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42931', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42931', 
	'Plantas e Fungos', 
	'Primeiros socorros em acidentes com plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42932', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43132', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43137', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43365', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43860', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'dados estatisticos nacionais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43933', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Acidente com serpentes em cachorros.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43966', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44074', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44141', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44194', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44240', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44374', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44537', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44569', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44635', 
	'Administrativa', 
	'Processos juridicos (Ministerio da Justica/Ministerio do Trabalho/Ministerio Publico)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44653', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44653', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44655', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44655', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45100', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45172', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45172', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45401', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45401', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45438', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Fabio Porcaro liga e solicita informacoes sobre a planta anador/melhoral. Pois gostaria de fazer uso desta para fim medicinal (cefaleia). Informo que a planta e usada para este fim, mas para informar sobre o manejo e quantidade usada para diversas finalidades (cha, xarope...) demandaria maior pesquisa. Ele solicita que o CIT-Biologia envie e-mail para o seu endereco com tais informacoes.E-mail: fabioporcaro@hotmail.comDeixo o CIT a disposicao.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45526', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45577', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45582', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45582', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45746', 
	'Medicamentos', 
	'Apresentacao/formulacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45955', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45959', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46205', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Avaliar diagnostico diferencial de lesao em face com 2 dias de evolucao'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46261', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46307', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46333', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46333', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46338', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46342', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Harpagophytm procumbens - garra do diabo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46461', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46491', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46491', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46503', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46522', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46755', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46755', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46889', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46916', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47006', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47259', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47482', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47482', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47542', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47557', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47632', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47632', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47837', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47918', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48047', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48076', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48086', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48103', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48131', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48134', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48134', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48151', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48151', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48271', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48302', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48305', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48305', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48308', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48308', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48343', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48343', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48480', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48750', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48750', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48812', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48831', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48831', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48855', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48855', 
	'Plantas e Fungos', 
	'Primeiros socorros em acidentes com plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49014', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49014', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49032', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49048', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49048', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49067', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49263', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49451', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49451', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49624', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49655', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50132', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50503', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50503', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50527', 
	'Outros', 
	'Informacoes sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50685', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50685', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50858', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51031', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51406', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51409', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51518', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51777', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51777', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51779', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51779', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51806', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51823', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'52221', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'52221', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53678', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Monografia acidente botropico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53843', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53843', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53892', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'54212', 
	'Administrativa', 
	'Outra informacao administrativa', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55071', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55171', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55291', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55445', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55625', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55625', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55627', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55642', 
	'Administrativa', 
	'Outra informacao administrativa', 
	'Informacoes sobre estoque e compra de antidotos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55642', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55721', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55728', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55728', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55914', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55917', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56048', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56206', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56206', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56844', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56885', 
	'Identificacao de Substancias', 
	'Identificacao de substancias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56928', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56947', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57064', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57064', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57129', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57129', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57504', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57504', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57529', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57529', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57757', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57782', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57811', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57811', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57852', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58255', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58903', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58903', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58905', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58905', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Envio de Lonomias ao I. Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59334', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59358', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59600', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Tipos de veneno que pode ocorrer paralisia gradativa em caes'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59970', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60012', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60816', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Envio de Lonomias ao CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61385', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61385', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Envio de Lonomias ao I.Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61595', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61595', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61610', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61769', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Sobre a possibilidade de doencas e como coletar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62308', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62345', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Teratogenicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62533', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62574', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62578', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62784', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62784', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62808', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62808', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63169', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63546', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64061', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64212', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64342', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64389', 
	'Identificacao de Substancias', 
	'Identificacao de substancias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64428', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre estatisticas de intoxicacao por medicamentos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64473', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Monografia'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64612', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64919', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64919', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64934', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'65302', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'65302', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66793', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66793', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66797', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66803', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66803', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66838', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66838', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66985', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66985', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67049', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67488', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67501', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67501', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67583', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67762', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67768', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67768', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67863', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67882', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68082', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68138', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68172', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68418', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68421', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68421', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68432', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68626', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68626', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68658', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68666', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68696', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68696', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68851', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68851', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69252', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69252', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69274', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69533', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69560', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69563', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Informacoes '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69615', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69615', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70127', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70144', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70232', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71164', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71659', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71697', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71983', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71993', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72106', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72241', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72389', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72398', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72405', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72405', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72461', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72818', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72861', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73469', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73713', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73786', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74141', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74192', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74317', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74786', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74786', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74963', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74963', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75286', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75313', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75360', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75552', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76073', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76073', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76544', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76615', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76897', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Recolhimento de cobras'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76982', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77477', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77793', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77793', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77805', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77874', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77894', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77894', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77913', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77913', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78044', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78170', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78170', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78375', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78375', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78641', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78641', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78657', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79036', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79036', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79144', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79632', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79632', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79747', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80170', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Solicitacao feita pelo CIT, de coleta de Lonomias e posterior envio ao I. Butantan.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80180', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Encaminhamento de coleta de Lonomia e envio ao CIT e posterior encaminhamento ao I. Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80209', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80515', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80659', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80672', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'81034', 
	'Medicamentos', 
	'Farmacocinetica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'81058', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82378', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82466', 
	'Administrativa', 
	'Processos juridicos (Ministerio da Justica/Ministerio do Trabalho/Ministerio Publico)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82466', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82578', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83432', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83432', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83556', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83562', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83834', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84258', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84261', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84263', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Principais medicamentos relacionados a intoxicacao e obito.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84269', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84269', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84273', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84337', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85088', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85122', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85122', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85572', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85858', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85858', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85935', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85935', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86120', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86164', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86919', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87315', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87343', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87625', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Informacoes sobre tratamento de acidentes com animais peconhentos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88185', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88321', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88489', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88489', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88769', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88793', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89004', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Auxilio no esclarecimento de obito.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89054', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89911', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89921', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89921', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90144', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90149', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90149', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90351', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90351', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90359', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90359', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90395', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90395', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90548', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90548', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90568', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Como remover animais da residencia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90591', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90591', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90633', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90633', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91330', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91330', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91444', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91551', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91860', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92456', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92700', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92700', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92735', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92832', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92962', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Coleta de Lonomias para envio ao Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93238', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93247', 
	'Outros', 
	'Informacoes sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93342', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93427', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93427', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93946', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93946', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93973', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93973', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93974', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93974', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94125', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94278', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94666', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94682', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94682', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94816', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95110', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95217', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95495', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95559', 
	'Informacao Ocupacional', 
	'Monitorizacao de toxicidade na rotina', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95560', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95560', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95634', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95745', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96660', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96661', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96662', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96760', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96760', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96780', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96795', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96795', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97007', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97048', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97325', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97552', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98093', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98119', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98414', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98423', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98714', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98716', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98926', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99205', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99205', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99229', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99231', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99319', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99319', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99672', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99714', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99714', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100232', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100488', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100541', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Solicita informacoes sobre transmissao de raiva por reptil.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100793', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101136', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101136', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101937', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102012', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102171', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102542', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102542', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102691', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102984', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103933', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104171', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104344', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104344', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104357', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104504', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104565', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'conduta em caso de contato com morcego'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104798', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105660', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105756', 
	'Medicamentos', 
	'Composicao/Apresentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105770', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106328', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106337', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106368', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106368', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106554', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106597', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106597', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106715', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106839', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106910', 
	'Medicamentos', 
	'Apresentacao/formulacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107130', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107130', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107248', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107399', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107399', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107601', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'108595', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'108912', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109103', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109854', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109872', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109872', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110112', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110112', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110369', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110695', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110746', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110746', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110920', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'111725', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'111967', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112072', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112214', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112214', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112246', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112246', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112312', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112314', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112500', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112530', 
	'Informacao Ocupacional', 
	'Manipulacao segura de substancias quimicas no trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112530', 
	'Informacao Ocupacional', 
	'Equipamentos de Protecao Coletiva (EPC)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113504', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113540', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113860', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114003', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114252', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114252', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114462', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115003', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115050', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115101', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115928', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115930', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116232', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116386', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116386', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116891', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117136', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117439', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117439', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117451', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118062', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118218', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118756', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119060', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119150', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Informacoes sobre tratamento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119347', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119347', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119627', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120113', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120791', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120791', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120909', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120930', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120930', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121022', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121022', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121024', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121029', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121029', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121034', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121058', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121304', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121629', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121629', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121633', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121668', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121668', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122235', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122235', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122290', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122526', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122533', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122642', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122801', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122801', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122842', 
	'Medicamentos', 
	'Farmacocinetica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123158', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123669', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124082', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Zuleika ligou para registrar que nao e so em Canasvieiras que tem escorpiao. No ano de 1990 menciona que morava em Ingleses, em uma casa de pedra no Morro do Mauricio e coletou um escorpiao que trouxe ao CIT/SC. A epoca disseram que ate entao nunca havia sido encontrado nenhum animal com essas caracteristicas. Ligou para deixar esse registro.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124131', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124398', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124433', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124596', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124829', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124892', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125240', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125732', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Fornecimento de soro '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125986', 
	'Identificacao de Substancias', 
	'Identificacao de substancias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125992', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126145', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126183', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126183', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126795', 
	'Administrativa', 
	'Outra informacao administrativa', 
	'Soro antielapidico e soro antibotropico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126795', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127572', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127662', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127870', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127954', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128135', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128160', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128160', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128213', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128363', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128637', 
	'Identificacao de Substancias', 
	'Identificacao de substancias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128677', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128692', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128733', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129285', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129381', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129389', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129481', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129954', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Conduta frente a diversos acidentes bothropicos ocorridos em um mesmo paciente '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129966', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129966', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130040', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130066', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130184', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130500', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131031', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131263', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131564', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131564', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131643', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131870', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131870', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131899', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131910', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132140', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Analgesico p-hidroxi-benzoato de viminol (Zambon)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132163', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132241', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132268', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132494', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132495', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132495', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132684', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133179', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133233', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133303', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133303', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133787', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133844', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Coleta e envio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134079', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134079', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134371', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134384', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134999', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135080', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135085', 
	'Medicamentos', 
	'Composicao/Apresentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135299', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135299', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135628', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135865', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135894', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'136140', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137607', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137668', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137680', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137792', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138078', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138089', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138288', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138288', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138365', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'139120', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'139120', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140986', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140986', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141292', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141292', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141396', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141396', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141585', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142477', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142683', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142721', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'orgao responsavel por recolher/coletar serpente dentro do domicilio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142750', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Colinesterase alterada (11.328)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142750', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143050', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143050', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143082', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143082', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143101', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143167', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143167', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143369', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143387', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143387', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'BENEFICIO DO USO DA PLANTA NO TRATAMENTO DO DIABETES'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143411', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143825', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144617', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144617', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144625', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144625', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144886', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144886', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145154', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145154', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145157', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145157', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145200', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145200', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145535', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145535', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145636', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146198', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146198', 
	'Raticidas', 
	'Informacoes sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146476', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146508', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146904', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146904', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147028', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147461', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147461', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147735', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147735', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147963', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147963', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147973', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'148409', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149670', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149670', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149688', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149688', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149832', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Investigacaodiagnostica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149997', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150359', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150609', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150609', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151043', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151276', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151373', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151373', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151629', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151968', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151968', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'152832', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153607', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153618', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153899', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153932', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153932', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153985', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Coleta de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154013', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154032', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154032', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154034', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154169', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154209', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Coleta de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154557', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154662', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154734', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155089', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155149', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155240', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155263', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155503', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155527', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155632', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155663', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155665', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155680', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155680', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155758', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156387', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156387', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156389', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156397', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156397', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156444', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Engano'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156847', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Sobre criterios de ampolas adicionais com acidente crotalico, alem de informacoes sobre consequencias neurologicas como persistencia de midriase e diplopia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157179', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157208', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Prevalencia e incidencia de medicacoes toxicas.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157241', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157360', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Acidente com perfuro cortantes em ambiente hospitalar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157481', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157641', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157641', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157642', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157731', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157761', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157766', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157852', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157852', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158122', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158305', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158470', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158476', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158506', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158512', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158513', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158513', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158764', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159122', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159449', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159449', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159487', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159501', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159616', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159781', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159781', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159848', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160007', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Dados estatisticos de intoxicacoes por medicamentos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160702', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160732', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160770', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160809', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Mordida de animal nao peconhento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160961', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161470', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161540', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161541', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Captura de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161598', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161772', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161772', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161851', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161855', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Conduta, prognostico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161921', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161955', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162062', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162064', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'uso de argila branca'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162183', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162399', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162399', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162437', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162502', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162642', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162799', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162898', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162898', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163524', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Uso da Amora para a menopausa'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163605', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163733', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'orientacao sobre mordedura canina'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163819', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164128', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164141', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164181', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164356', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164378', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164387', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164643', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164982', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165601', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165671', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Retirada de animal encontrado na residencia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165672', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165678', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165741', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165751', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165925', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Amostra enviada sem contato previo com o CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165961', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166219', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166222', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166572', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'MECANISMO DE ACAO'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166572', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166748', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167383', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167547', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167547', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167631', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Contato com gato'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167910', 
	'Drogas de Abuso', 
	'Centros de referencia para dependentes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168160', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168169', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168263', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168285', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Deseja saber sobre os usos de uma planta que leu ser moduladora celular (ajuda no cancer)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168417', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Encaminhamento do paciente'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168467', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168467', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168727', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168736', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168774', 
	'Informacao Toxicologica', 
	'Teratogenicidade', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169400', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169498', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Medidas de profilaxia '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169581', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169649', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'zika virus e dengue'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169875', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169875', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169970', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170012', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170164', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170261', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170278', 
	'Raticidas', 
	'Informacoes sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170278', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170397', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170540', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170627', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170627', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171011', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171014', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171137', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171149', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171149', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171253', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171739', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171760', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171760', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171791', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171839', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172120', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172428', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172429', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre o atendimento do CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172529', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172609', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172630', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172698', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172903', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172910', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173240', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173361', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173681', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174127', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174274', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174274', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174304', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174307', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174382', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174822', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Uso de anti inflamatorios associados'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174874', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174933', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Animais peconhentos no Alto Vale de SC '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174993', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Se medicamento e confiavel'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175107', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175355', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175372', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175561', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175614', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175673', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175674', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175694', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175703', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'conduta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175746', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175861', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175926', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175926', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176071', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176344', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Toxicidade de gas de explosao de Bateria de no break'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176344', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176398', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176612', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176612', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176636', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176774', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176774', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176933', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176961', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177266', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177401', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177466', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177473', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177530', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177564', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177606', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178101', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178101', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178328', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178496', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178496', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178577', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178587', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178663', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178743', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Informacoes sobre dose toxica '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178774', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178774', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178793', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179218', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179223', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179359', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre uso de catarticos / carvao ativado em doses multiplas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179448', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Informacao sobre toxicidade da fumaca de Parafina, Xilol e Formol'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179455', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179620', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179751', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Intoxicacoes com Triciclicos e Benzodiazepinicos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Cuidados para nao ter acidente.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179876', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180205', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180351', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180525', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Mordedura por cao'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180541', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181078', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181078', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181456', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Mordida de cachorro'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181721', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181819', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182013', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182081', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Habitat e habitos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182340', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Cha de Palmeirinha - Eleutherine bulbosa'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182592', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182841', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182852', 
	'Administrativa', 
	'Outra informacao administrativa', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183181', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'quer informacoes terapeuticas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183337', 
	'Medicamentos', 
	'Compatibilidade de medicacao parenteral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183505', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183612', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183924', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183994', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184549', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184549', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184597', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Soro antiofidico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184943', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184943', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185035', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185385', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185491', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185568', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185568', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185727', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185738', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185830', 
	'Outros', 
	'Informacao sobre doencas gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185891', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186598', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Identificacao de composicao de formula'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186626', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186785', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186785', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187314', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187368', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187399', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187779', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187795', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187813', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188273', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'sobre lote na lote de salox aplicado a 4 anos em paciente.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188321', 
	'Raticidas', 
	'Informacoes sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188327', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188362', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188446', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188446', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188507', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Preenchimento campo circunstancia no DATATOX'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188661', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188750', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188752', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188993', 
	'Administrativa', 
	'Outra informacao administrativa', 
	'Informacoes sobre Soro'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'189012', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'189462', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190028', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190070', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190083', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190110', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Locais de atendimento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190294', 
	'Medicamentos', 
	'Contra-indicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190635', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190674', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Dose Toxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190683', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190843', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190883', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190916', 
	'Medicamentos', 
	'Interacoes farmaco-alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190925', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'191076', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192091', 
	'Outros', 
	'Informacoes sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192614', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192807', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192877', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193671', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193717', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193786', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193890', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194003', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194300', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194601', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194601', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195316', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195431', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195431', 
	'Medicamentos', 
	'Farmacocinetica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195458', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Protocolo de atendimento em intoxicacao por raticidas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195908', 
	'Identificacao de Substancias', 
	'Identificacao de formas farmaceuticas solidas (sob prescricao, isentas de prescricao, suplemento dietetico/ervas, ilicita)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196047', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196395', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196424', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196775', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196775', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196845', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197469', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197475', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197485', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Fitoterapicos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197612', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197913', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'198262', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199211', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199221', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199427', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199717', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201051', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201092', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201441', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201468', 
	'Produtos de Uso Veterinario', 
	'Informacoes sobre produtos de uso veterinario', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201718', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Mordedura de gato'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201827', 
	'Medicamentos', 
	'Farmacologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201847', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202079', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202138', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202350', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202377', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202666', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202727', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202727', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203257', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203474', 
	'Metais', 
	'Informacoes sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203539', 
	'Outros', 
	'Informacao sobre doencas gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204236', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204344', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204381', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204388', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Necessidade de conducao para hospital'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204992', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204999', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'205786', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'205900', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'206043', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'206940', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207311', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207654', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207827', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207827', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207852', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207922', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208298', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Piper Longum - pode ser utilizada na gestacao?  Cadastrar. '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208872', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208896', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'209742', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'209802', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'210375', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211079', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211192', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Dose toxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211381', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211755', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211862', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211862', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'212303', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'212564', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213036', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213296', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213296', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213379', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'214257', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'214891', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215086', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215130', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215237', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215455', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215653', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216862', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216947', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216989', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217428', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217434', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217693', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217769', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217773', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Como retirar o ferrao da abelha em caso de acidente'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217874', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217886', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219053', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219182', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219656', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219690', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219690', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220062', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220115', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220328', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221106', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221255', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221789', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'222066', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'222235', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223088', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223138', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223239', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223448', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223748', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223775', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224283', 
	'Medicamentos', 
	'Composicao/Apresentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224532', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224847', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225288', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225314', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225314', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225325', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225392', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225450', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225450', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225600', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225600', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225763', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225763', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225799', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226317', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226317', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226936', 
	'Medicamentos', 
	'Composicao/Apresentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226936', 
	'Medicamentos', 
	'Apresentacao/formulacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227177', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227177', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227182', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'228894', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'228903', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229527', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229735', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229754', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229875', 
	'Medicamentos', 
	'Contra-indicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230375', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230383', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230430', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230430', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230810', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230840', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230843', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231115', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231668', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231678', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232001', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232210', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232719', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233052', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233052', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233140', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234281', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234281', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234331', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234336', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234639', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234640', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234640', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234684', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234846', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234846', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234868', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234868', 
	'Administrativa', 
	'Pesquisa na base de dados do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235177', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235678', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235878', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236382', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236556', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236556', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236560', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236838', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236891', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237016', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237111', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237431', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237431', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237736', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238487', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238487', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238674', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239101', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239126', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239301', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239429', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239429', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239770', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240557', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240631', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240987', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241201', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241532', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241882', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentacao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242343', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242381', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242383', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242425', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242634', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242815', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243078', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243078', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243251', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243472', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243729', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'244826', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'245617', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'245628', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246054', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246054', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246203', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246203', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246283', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Soro loxoscelico antiveneno'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247302', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247847', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247939', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247939', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248156', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248177', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248211', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248211', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248518', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248696', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248915', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249045', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249088', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249088', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249139', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249153', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249181', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249207', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249337', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249491', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249492', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249503', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249503', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249518', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Como eliminar loxosceles'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249519', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249718', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Encaminhar lonomias'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249873', 
	'Medicamentos', 
	'Dose terapeutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249893', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249893', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249916', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249926', 
	'Informacao Ocupacional', 
	'Dados tecnicos sobre seguranca e manuseio de material (ficha tecnica)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249931', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250160', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250203', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250345', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250488', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250608', 
	'Informacao Ocupacional', 
	'Informacoes sobre os produtos quimicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250853', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251244', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251350', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251700', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252051', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252051', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252400', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252712', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252712', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252803', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252830', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253834', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253931', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254045', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254079', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254083', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254083', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254152', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254400', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254407', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254500', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254500', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254750', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254759', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254817', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255838', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255948', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255948', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256130', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256194', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256194', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256203', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256203', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256241', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256855', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256934', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256946', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257098', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257219', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257309', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257425', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257494', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257598', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258034', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258080', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258142', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258156', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258308', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258494', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258683', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258693', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258851', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258865', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258942', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259298', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259579', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260825', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260869', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260951', 
	'Outros', 
	'Informacao sobre doencas gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261125', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261125', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261545', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261679', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Hepatotoxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261682', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261682', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261899', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262007', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262007', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262345', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262345', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262402', 
	'Raticidas', 
	'Informacoes sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262526', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263151', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263155', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263209', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre realizacao de exames'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263245', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263272', 
	'Informacao Ocupacional', 
	'Outra informacao ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263272', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263347', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263347', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263579', 
	'Identificacao de Substancias', 
	'Identificacao de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263707', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263785', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263801', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263806', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263806', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263913', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263913', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264148', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264148', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264239', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265101', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265375', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265674', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265997', 
	'Medicamentos', 
	'Estabilidade/armazenamento', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266444', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266444', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266801', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266829', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Capturou animal e quer soltar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266852', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267323', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267323', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267513', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Avaliar hipotese de exposicao e conduta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267648', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267818', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268061', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268173', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268173', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268249', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268522', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268522', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268808', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268808', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268816', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269342', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269709', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269741', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269744', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270018', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270018', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270278', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270450', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270613', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270613', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271030', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271030', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271061', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271108', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271762', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272234', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272330', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272566', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272566', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272732', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272812', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272825', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272882', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272954', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273165', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273785', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273952', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274063', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274672', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274786', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Conduta frente a acidente com abelhas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274876', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275123', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275145', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275373', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275644', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276187', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276208', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276361', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276529', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276859', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277038', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277041', 
	'Administrativa', 
	'Informacoes sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277472', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277608', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278252', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278485', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278485', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278744', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'279200', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'279234', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Uso de pralidoxima no Brasil'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280090', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280222', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280222', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280229', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280251', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280251', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280289', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280289', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280402', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280402', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280676', 
	'Outros', 
	'Apoio para eventos nao toxicologicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280803', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281149', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281149', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281153', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281153', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281187', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281196', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281257', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281567', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281649', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282204', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282204', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282656', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282838', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacoes a respeito da toxicidade da Melaleuca para uso na agricultura.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283047', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283127', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283470', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284204', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284204', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284264', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284350', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284482', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284619', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Protocolos de Atendimento em exposicao a Barbituricos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284848', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284855', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284855', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284956', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284956', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284998', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285532', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285656', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285683', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285969', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285969', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286089', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286096', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286247', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286247', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286435', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286522', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'287469', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'287733', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288301', 
	'Administrativa', 
	'Solicitacao de 0800 para o rotulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288339', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288344', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288370', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289050', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289050', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289241', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289288', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289355', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289355', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289540', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289619', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289832', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289846', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289954', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Coleta e descarte de substancia desconhecida'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289956', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290240', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290248', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre obito por lonomia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290514', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290877', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'291047', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'291487', 
	'Outros', 
	'Apoio para eventos nao toxicologicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292051', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292373', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292373', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292387', 
	'Informacao Ocupacional', 
	'Informacao sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292875', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292972', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293199', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293574', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293574', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293578', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293783', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293825', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294065', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294070', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294854', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295067', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295348', 
	'Administrativa', 
	'Processos juridicos (Ministerio da Justica/Ministerio do Trabalho/Ministerio Publico)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295496', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296477', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296479', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296764', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Doacao de aranha para disciplina'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296845', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297632', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297721', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297737', 
	'Raticidas', 
	'Informacoes sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297795', 
	'Administrativa', 
	'Processos juridicos (Ministerio da Justica/Ministerio do Trabalho/Ministerio Publico)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297929', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298392', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298392', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298406', 
	'Medicamentos', 
	'Indicacoes/Uso terapeutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'299321', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'299413', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300213', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300213', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300219', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300219', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300230', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Uso, identificacao, dose e toxicologia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300663', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'301409', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'301409', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302495', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302495', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302579', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302870', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303286', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303286', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303658', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303930', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304015', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304063', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304757', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304823', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304823', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304851', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305176', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305354', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305354', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305394', 
	'Administrativa', 
	'Outra informacao administrativa', 
	'Envio de amostra para NARTAD'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306859', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306883', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307255', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307267', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307390', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307657', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	'Informacoes sobre atividade terapeutica da planta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307715', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308048', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308538', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308863', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308895', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Uso Toxbase'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309258', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309258', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309258', 
	'Alimentos', 
	'Informacoes sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309258', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309333', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309354', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Dados estatisticos registrados pelo CIT no ano de 2016 por mes nos acidentes com animais peconhentos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309650', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'309879', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'310170', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'310364', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'310487', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'310741', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'310833', 
	'Administrativa', 
	'Pedidos de parecer, pericia ou declaracao', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311004', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311177', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311457', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311487', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311487', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311529', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311575', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311697', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311720', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311764', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311816', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'311960', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312192', 
	'Administrativa', 
	'Pesquisa na base de dados do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312767', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312778', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312778', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312846', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312849', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312926', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'312929', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313104', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313117', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313434', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313468', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'313996', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'314079', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'314136', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	'Disturbios acido-basicos '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'314827', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'315402', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'doacao de amostras mortas de lagarta Lonomia e mariposa'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'315548', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'315578', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'315610', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'315780', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'316093', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317306', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317409', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'palestra'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317426', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317426', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317512', 
	'Informacao Toxicologica', 
	'Condutas terapeuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317672', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317691', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317716', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'317804', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'318332', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'318574', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319248', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319248', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319406', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319406', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319406', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319477', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319477', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319556', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319675', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319774', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'319774', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320046', 
	'Drogas de Abuso', 
	'Triagem toxicologica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320062', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320209', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320288', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320289', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'320837', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'321974', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322100', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322177', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322339', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322383', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322519', 
	'Administrativa', 
	'Estatisticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322527', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322527', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322697', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'322723', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'323113', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'323136', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'323181', 
	'Drogas de Abuso', 
	'Informacoes sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'323462', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324047', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324079', 
	'Informacao Toxicologica', 
	'Referencias bibliograficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324079', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324581', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324734', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'324977', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'325318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'325318', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'325555', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'325661', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'325667', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'326274', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'326409', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327090', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327090', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327148', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327148', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327171', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327501', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327799', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'327799', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328215', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328215', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328215', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328702', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328702', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328789', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'328958', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'329487', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'329701', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'329723', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330118', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330118', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330229', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330229', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330687', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'330890', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331105', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331295', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331319', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331744', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331744', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331887', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'331887', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'333272', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'333593', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'333719', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'333812', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'334247', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'334318', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'334396', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'335637', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'335637', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'335637', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'335718', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'335893', 
	'Medicamentos', 
	'Administracao de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'336994', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'336994', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337118', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337118', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337160', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337160', 
	'Informacao Ambiental', 
	'Contaminacao ambiental', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337564', 
	'Informacao Toxicologica', 
	'Prevencao/Seguranca/Informacoes educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'337695', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'338601', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'338641', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'338670', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'338811', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340128', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340165', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340280', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340421', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340443', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'340702', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'341212', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'341685', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'341685', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'341685', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342213', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342213', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342213', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342467', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342467', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Emprestimo de kit de animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342493', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'342854', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'343227', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'343850', 
	'Informacao Toxicologica', 
	'Primeiros socorros em intoxicacoes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'343914', 
	'Informacao Ambiental', 
	'Contaminacao ambiental', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344063', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344141', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344297', 
	'Drogas de Abuso', 
	'Efeitos de substancias ilicitas - sem vitima(s) identificada(s)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344610', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344617', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344716', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344716', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'344749', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'345658', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'345856', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'346072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'346072', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'346861', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347119', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347171', 
	'Agrotoxicos', 
	'Informacoes sobre agrotoxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347173', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347193', 
	'Cosmeticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informacoes sobre cosmeticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347836', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'347860', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'348061', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'348390', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'348538', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'348617', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'348809', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'349056', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'349056', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'349842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'349842', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'349888', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350050', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350059', 
	'Medicamentos', 
	'Farmacologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350242', 
	'Informacao Toxicologica', 
	'Outra informacao toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350429', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350432', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350432', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'350947', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351000', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351000', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351007', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351007', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351296', 
	'Metais', 
	'Informacoes sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351343', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351374', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351415', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'351752', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'352345', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'352648', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'352747', 
	'Medicamentos', 
	'Outras informacoes sobre medicamentos', 
	'Toxicologia Ibuprofeno'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'352891', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353174', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353185', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353587', 
	'Informacao Toxicologica', 
	'Doses toxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353648', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353694', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353694', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'353800', 
	'Medicamentos', 
	'Antidotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354060', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354060', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354060', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354182', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354271', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354297', 
	'Informacao Toxicologica', 
	'Analise toxicologica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354447', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Primeiros socorros em animais peconhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'354474', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355038', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355074', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355146', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355424', 
	'Produtos Quimicos de Uso Residencial/Industrial', 
	'Informacoes sobre produtos quimicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355424', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'355424', 
	'Informacao Toxicologica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356003', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356032', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356129', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356129', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356563', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356572', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356687', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356687', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'356697', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'357078', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'357078', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'357339', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358552', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358552', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358589', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358630', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358630', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358642', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358660', 
	'Medicamentos', 
	'Interacoes medicamentosas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'358902', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Outra informacao sobre animais', 
	'Mordedura de cachorro'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359096', 
	'Administrativa', 
	'Palestra, aula, curso, seminario, simposio, congresso, webconferencia, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359208', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359259', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359697', 
	'Outros', 
	'Outro tipo de informacao nao classificada', 
	'Informacao sobre vacinas: tifoide, febre amarela e hepatite A'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359852', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359876', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'359876', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'360189', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'360282', 
	'Administrativa', 
	'Emprestimo de material didatico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'360549', 
	'Medicamentos', 
	'Reacoes adversas (sem exposicao conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'361366', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'361465', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'361827', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'361937', 
	'Produtos Domissanitarios', 
	'Informacoes sobre produtos domissanitarios', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362466', 
	'Plantas e Fungos', 
	'Outra informacao sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362466', 
	'Plantas e Fungos', 
	'Identificacao de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362812', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362815', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362912', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362949', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362949', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'362967', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'363007', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'363673', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'364729', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'365370', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'365387', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Informacoes sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'365388', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'365924', 
	'Inseticidas de Uso Domestico', 
	'Informacoes sobre inseticidas de uso domestico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'366173', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'366182', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'366441', 
	'Animais Peconhentos/Venenosos e Animais Nao Peconhentos/Nao Venenosos', 
	'Identificacao de animal', 
	''
);

