CREATE TABLE IF NOT EXISTS intoxicacao_db.05_exposicao (
	identificador_caso int,
	data_exposicao datetime,
	horario_exposicao varchar(1024),
	tempo_decorrido varchar(1024),
	especificacao_tempo_decorrido varchar(1024),
	tempo_exposicao varchar(1024),
	especificacao_tempo_exposicao varchar(1024),
	intensidade_exposicao varchar(1024),
	local_exposicao varchar(1024),
	zona_exposicao varchar(1024),
	cidade_exposicao varchar(1024),
	estado_exposicao varchar(1024),
	pais_exposicao varchar(1024),
	via_exposicao varchar(1024),
	circunstancia_exposicao varchar(1024),
	intencao_exposicao varchar(1024)
);
