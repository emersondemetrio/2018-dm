CREATE TABLE IF NOT EXISTS intoxicacao_db.04_paciente (
	identificador_caso int,
	idade varchar(1024),
	especificacao_idade varchar(1024),
	periodo_gestacao varchar(1024),
	peso varchar(1024),
	descricao varchar(1024),
	cidade_residencia varchar(1024),
	estado_residencia varchar(1024),
	profissao varchar(1024),
	sexo varchar(1024),
	raca_etnia varchar(1024),
	data_nascimento datetime
);
