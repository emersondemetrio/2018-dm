CREATE TABLE IF NOT EXISTS intoxicacao_db.14_paciente_animal (
	identificador_caso int,
	data_nascimento datetime,
	idade varchar(1024),
	especificacao_idade varchar(1024),
	sexo varchar(1024),
	peso varchar(1024),
	cidade_residencia varchar(1024),
	estado_residencia varchar(1024),
	pais_residencia varchar(1024),
	especie varchar(1024),
	complemento_especie varchar(1024),
	raca varchar(1024),
	complemento_raca varchar(1024)
);
