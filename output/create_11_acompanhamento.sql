CREATE TABLE IF NOT EXISTS intoxicacao_db.11_acompanhamento (
	identificador_caso int,
	data_acompanhamento datetime,
	classificacao_informante varchar(1024),
	cidade_origem_informacao varchar(1024),
	estado_origem_informacao varchar(1024),
	pais_origem_informacao varchar(1024),
	evolucao varchar(1024),
	complemento varchar(1024)
);
