CREATE TABLE IF NOT EXISTS intoxicacao_db.12_encerramento (
	identificador_caso int,
	data_encerramento datetime,
	internacao varchar(1024),
	tempo_internacao varchar(1024),
	especificacao_tempo_internacao varchar(1024),
	local_internacao varchar(1024),
	motivo_nao_internacao varchar(1024),
	desfecho varchar(1024),
	data_obito datetime,
	contribuicao_obito varchar(1024),
	autopsia varchar(1024),
	resultado_autopsia varchar(1024),
	cid10 varchar(1024),
	observacao varchar(1024)
);
